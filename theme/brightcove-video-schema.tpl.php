<div itemprop="video" itemscope itemtype="http://schema.org/VideoObject">
  <meta itemprop="name" content="<?php print $title; ?>" />
  <meta itemprop="duration" content="<?php print $duration; ?>" />
  <meta itemprop="thumbnailUrl" content="<?php print $thumbnail_url; ?>" />
  <meta itemprop="embedURL" content="<?php print $embed_url; ?>" />
  <meta itemprop="uploadDate" content="<?php print $created; ?>" />
  <?php print $video_object; ?>
  <meta itemprop="description"><?php print $description; ?></meta>
</div>
